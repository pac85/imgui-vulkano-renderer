mod shader;

use vulkano::command_buffer::RenderPassBeginInfo;
use vulkano::command_buffer::{
    pool::CommandPoolBuilderAlloc, sys::*, AutoCommandBufferBuilder, PrimaryAutoCommandBuffer,
};
use vulkano::descriptor_set::{DescriptorSet, PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::{Device, Queue};
use vulkano::pipeline::{
    graphics::{input_assembly::IndexType, GraphicsPipeline, GraphicsPipelineCreationError},
    Pipeline, PipelineBindPoint,
};
use vulkano::shader::ShaderStages;
use vulkano::sync::{AccessFlags, GpuFuture};
use vulkano::{
    buffer::{BufferAccess, BufferUsage, CpuBufferPool},
    command_buffer::SubpassContents,
};

use vulkano::image::{AttachmentImage, ImageDimensions, ImmutableImage};
use vulkano::sampler::{Sampler, SamplerCreateInfo};
// use vulkano::sampler::{Sampler, SamplerAddressMode, Filter, MipmapMode};
use vulkano::format::{ClearValue, Format};
use vulkano::pipeline::graphics::viewport::{Scissor, Viewport};
use vulkano::render_pass::{Framebuffer, FramebufferCreateInfo};
use vulkano::render_pass::{RenderPass, Subpass};

use bytemuck::{Pod, Zeroable};

use vulkano::image::ImageAccess;
use vulkano::image::ImageViewAbstract;

use std::convert::TryFrom;
use std::fmt;
use std::sync::Arc;

use imgui::{
    internal::RawWrapper, DrawCmd, DrawCmdParams, DrawVert, ImString, TextureId, Textures,
};

#[derive(Default, Debug, Clone, Copy, Pod, Zeroable)]
#[repr(C)]
struct Vertex {
    pub pos: [f32; 2],
    pub uv: [f32; 2],
    pub col: u32,
    // pub col: [u8; 4],
}

vulkano::impl_vertex!(Vertex, pos, uv, col);

impl From<DrawVert> for Vertex {
    fn from(v: DrawVert) -> Vertex {
        unsafe { std::mem::transmute(v) }
    }
}

#[derive(Debug)]
pub enum RendererError {
    GraphicsPipelineCreationError(GraphicsPipelineCreationError),
    DeviceMemoryAllocError(vulkano::memory::DeviceMemoryAllocationError),
    OomError(vulkano::OomError),
    DrawIndexedError(vulkano::command_buffer::DrawIndexedError),
    PersistentDescriptorSetError(vulkano::descriptor_set::DescriptorSetCreationError),
    /*PersistentDescriptorSetBuildError(
        vulkano::descriptor_set::PersistentDescriptorSetBuildError,
    ),*/
    FlushError(vulkano::sync::FlushError),
    SamplerCreationError(vulkano::sampler::SamplerCreationError),
    ImageCreationError(vulkano::image::ImageCreationError),
    ImmutableImageCreationError(vulkano::image::immutable::ImmutableImageCreationError),
    BeginRenderPassError(vulkano::command_buffer::RenderPassError),
    AutoCommandBufferBuilderContextError(
        vulkano::command_buffer::AutoCommandBufferBuilderContextError,
    ),
    WriteLockError(vulkano::buffer::cpu_access::WriteLockError),
    FramebufferCreationError(vulkano::render_pass::FramebufferCreationError),
    CopyBufferError(vulkano::command_buffer::CopyError),
    BadTexture(TextureId),
    BadImageDimensions(ImageDimensions),
}

impl fmt::Display for RendererError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            &Self::GraphicsPipelineCreationError(ref e) => {
                write!(f, "Error creating rendering pipeline: {}", e)
            }
            &Self::DeviceMemoryAllocError(ref e) => {
                write!(f, "Error allocating memory on the device: {}", e)
            }
            &Self::OomError(ref e) => {
                write!(f, "Out of memory: {}", e)
            }
            &Self::DrawIndexedError(ref e) => {
                write!(f, "Error creating draw_indexed command: {}", e)
            }
            &Self::PersistentDescriptorSetError(ref e) => {
                write!(f, "Error creating descriptor set: {}", e)
            }
            /*&Self::PersistentDescriptorSetBuildError(ref e) => {
                write!(f, "Error error building descriptor set: {}", e)
            }*/
            &Self::FlushError(ref e) => {
                write!(f, "Error flushing futures: {}", e)
            }
            &Self::SamplerCreationError(ref e) => {
                write!(f, "Error creating sampler: {}", e)
            }
            &Self::ImageCreationError(ref e) => {
                write!(f, "Error creating image: {}", e)
            }
            &Self::ImmutableImageCreationError(ref e) => {
                write!(f, "Error creating image: {}", e)
            }
            &Self::BeginRenderPassError(ref e) => {
                write!(f, "Error starting render pass: {}", e)
            }
            &Self::AutoCommandBufferBuilderContextError(ref e) => {
                write!(f, "Error adding to command buffer: {}", e)
            }
            &Self::WriteLockError(ref e) => {
                write!(f, "Could not acquire buffer write lock: {}", e)
            }
            &Self::FramebufferCreationError(ref e) => {
                write!(f, "Error creating framebuffer {}", e)
            }
            &Self::CopyBufferError(ref e) => {
                write!(f, "Error copying buffer: {:?}", e)
            }
            &Self::BadTexture(ref t) => {
                write!(f, "The Texture ID could not be found: {:?}", t)
            }
            &Self::BadImageDimensions(d) => {
                write!(f, "Image Dimensions not supported (must be Dim2d): {:?}", d)
            }
        }
    }
}

impl std::error::Error for RendererError {}

impl From<GraphicsPipelineCreationError> for RendererError {
    fn from(e: GraphicsPipelineCreationError) -> Self {
        RendererError::GraphicsPipelineCreationError(e)
    }
}
impl From<vulkano::memory::DeviceMemoryAllocationError> for RendererError {
    fn from(e: vulkano::memory::DeviceMemoryAllocationError) -> Self {
        RendererError::DeviceMemoryAllocError(e)
    }
}
impl From<vulkano::OomError> for RendererError {
    fn from(e: vulkano::OomError) -> Self {
        RendererError::OomError(e)
    }
}
impl From<vulkano::command_buffer::DrawIndexedError> for RendererError {
    fn from(e: vulkano::command_buffer::DrawIndexedError) -> Self {
        RendererError::DrawIndexedError(e)
    }
}
impl From<vulkano::descriptor_set::DescriptorSetCreationError> for RendererError {
    fn from(e: vulkano::descriptor_set::DescriptorSetCreationError) -> Self {
        RendererError::PersistentDescriptorSetError(e)
    }
}
/*impl From<vulkano::descriptor_set::PersistentDescriptorSetBuildError>
    for RendererError
{
    fn from(e: vulkano::descriptor_set::PersistentDescriptorSetBuildError) -> Self {
        RendererError::PersistentDescriptorSetBuildError(e)
    }
}*/
impl From<vulkano::sync::FlushError> for RendererError {
    fn from(e: vulkano::sync::FlushError) -> Self {
        RendererError::FlushError(e)
    }
}
impl From<vulkano::sampler::SamplerCreationError> for RendererError {
    fn from(e: vulkano::sampler::SamplerCreationError) -> Self {
        RendererError::SamplerCreationError(e)
    }
}
impl From<vulkano::command_buffer::RenderPassError> for RendererError {
    fn from(e: vulkano::command_buffer::RenderPassError) -> Self {
        RendererError::BeginRenderPassError(e)
    }
}
impl From<vulkano::command_buffer::AutoCommandBufferBuilderContextError> for RendererError {
    fn from(e: vulkano::command_buffer::AutoCommandBufferBuilderContextError) -> Self {
        RendererError::AutoCommandBufferBuilderContextError(e)
    }
}
impl From<vulkano::buffer::cpu_access::WriteLockError> for RendererError {
    fn from(e: vulkano::buffer::cpu_access::WriteLockError) -> Self {
        RendererError::WriteLockError(e)
    }
}
impl From<vulkano::render_pass::FramebufferCreationError> for RendererError {
    fn from(e: vulkano::render_pass::FramebufferCreationError) -> Self {
        RendererError::FramebufferCreationError(e)
    }
}
impl From<vulkano::image::ImageCreationError> for RendererError {
    fn from(e: vulkano::image::ImageCreationError) -> Self {
        RendererError::ImageCreationError(e)
    }
}
impl From<vulkano::image::immutable::ImmutableImageCreationError> for RendererError {
    fn from(e: vulkano::image::immutable::ImmutableImageCreationError) -> Self {
        RendererError::ImmutableImageCreationError(e)
    }
}
impl From<vulkano::command_buffer::CopyError> for RendererError {
    fn from(e: vulkano::command_buffer::CopyError) -> Self {
        RendererError::CopyBufferError(e)
    }
}

pub type Texture = (Arc<dyn ImageViewAbstract + Send + Sync>, Arc<Sampler>);

pub struct Renderer {
    render_pass: Arc<RenderPass>,
    pipeline: Arc<GraphicsPipeline>,
    font_texture: Texture,
    textures: Textures<Texture>,
    ds: Vec<Arc<dyn DescriptorSet>>,
    buffers: Vec<Arc<dyn BufferAccess>>,
    framebuffer: Option<Arc<Framebuffer>>,
    vrt_buffer_pool: CpuBufferPool<Vertex>,
    idx_buffer_pool: CpuBufferPool<u16>,
}

impl Renderer {
    /// Initialize the renderer object, including vertex buffers, ImGui font textures,
    /// and the Vulkan graphics pipeline.
    ///
    /// ---
    ///
    /// `ctx`: the ImGui `Context` object
    ///
    /// `device`: the Vulkano `Device` object for the device you want to render the UI on.
    ///
    /// `queue`: the Vulkano `Queue` object for the queue the font atlas texture will be created on.
    ///
    /// `format`: the Vulkano `Format` that the render pass will use when storing the frame in the target image.
    pub fn init(
        ctx: &mut imgui::Context,
        device: Arc<Device>,
        queue: Arc<Queue>,
        format: Format,
    ) -> Result<Renderer, RendererError> {
        let vs = shader::vs::load(device.clone()).unwrap();
        let fs = shader::fs::load(device.clone()).unwrap();

        let render_pass = vulkano::single_pass_renderpass!(
            device.clone(),
            attachments: {
                color: {
                    load: Load,
                    store: Store,
                    format: format,
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        )
        .unwrap();

        let pipeline = GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .vertex_shader(vs.entry_point("main").unwrap(), ())
            .triangle_list()
            .viewports_scissors_dynamic(1)
            .fragment_shader(fs.entry_point("main").unwrap(), ())
            .blend_alpha_blending()
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .build(device.clone())?;

        let textures = Textures::new();

        let font_texture = Self::upload_font_texture(ctx.fonts(), device.clone(), queue.clone())?;

        ctx.set_renderer_name(Some(format!(
            "imgui-vulkano-renderer {}",
            env!("CARGO_PKG_VERSION")
        )));

        let vrt_buffer_pool =
            CpuBufferPool::new(device.clone(), BufferUsage::vertex_buffer_transfer_dst());
        let idx_buffer_pool =
            CpuBufferPool::new(device.clone(), BufferUsage::index_buffer_transfer_dst());

        Ok(Renderer {
            render_pass,
            pipeline: pipeline,
            font_texture,
            textures,
            ds: vec![],
            buffers: vec![],
            framebuffer: None,
            vrt_buffer_pool,
            idx_buffer_pool,
        })
    }

    /// Appends the draw commands for the UI frame to an `AutoCommandBufferBuilder`.
    ///
    /// ---
    ///
    /// `cmd_buf_builder`: An `AutoCommandBufferBuilder` from vulkano to add commands to
    ///
    /// `device`: the Vulkano `Device` object for the device you want to render the UI on
    ///
    /// `queue`: the Vulkano `Queue` object for buffer creation
    ///
    /// `target`: the target image to render to
    ///
    /// `draw_data`: the ImGui `DrawData` that each UI frame creates
    pub fn draw_commands<I, P>(
        &mut self,
        cmd_buf_builder: &mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer<P::Alloc>, P>,
        _queue: Arc<Queue>,
        target: I,
        draw_data: &imgui::DrawData,
    ) -> Result<(), RendererError>
    where
        I: ImageViewAbstract + Send + Sync + 'static,
        P: CommandPoolBuilderAlloc,
    {
        let fb_width = draw_data.display_size[0] * draw_data.framebuffer_scale[0];
        let fb_height = draw_data.display_size[1] * draw_data.framebuffer_scale[1];
        if !(fb_width > 0.0 && fb_height > 0.0) {
            return Ok(());
        }
        let left = draw_data.display_pos[0];
        let right = draw_data.display_pos[0] + draw_data.display_size[0];
        let top = draw_data.display_pos[1];
        let bottom = draw_data.display_pos[1] + draw_data.display_size[1];

        let pc = shader::vs::ty::VertPC {
            matrix: [
                [(2.0 / (right - left)), 0.0, 0.0, 0.0],
                [0.0, (2.0 / (bottom - top)), 0.0, 0.0],
                [0.0, 0.0, -1.0, 0.0],
                [
                    (right + left) / (left - right),
                    (top + bottom) / (top - bottom),
                    0.0,
                    1.0,
                ],
            ],
        };

        let dims = match target.image().dimensions() {
            ImageDimensions::Dim2d { width, height, .. } => [width, height],
            d => {
                return Err(RendererError::BadImageDimensions(d));
            }
        };

        let viewports = vec![Viewport {
            origin: [0.0, 0.0],
            dimensions: [dims[0] as f32, dims[1] as f32],
            depth_range: 0.0..1.0,
        }];
        let mut scissors = vec![Scissor::default()];

        let clip_off = draw_data.display_pos;
        let clip_scale = draw_data.framebuffer_scale;

        let layout = &self.pipeline.layout().set_layouts()[0];

        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![Arc::new(target)],
                ..Default::default()
            },
        )?;

        let render_pass_nbegin_info = RenderPassBeginInfo {
            clear_values: vec![None],
            ..RenderPassBeginInfo::framebuffer(framebuffer)
        };

        cmd_buf_builder.begin_render_pass(render_pass_nbegin_info, SubpassContents::Inline)?;

        for draw_list in draw_data.draw_lists() {
            let vertex_buffer = self
                .vrt_buffer_pool
                .chunk(draw_list.vtx_buffer().iter().map(|&v| Vertex::from(v)))
                .unwrap();
            let index_buffer = self
                .idx_buffer_pool
                .chunk(draw_list.idx_buffer().iter().cloned())
                .unwrap();

            for cmd in draw_list.commands() {
                match cmd {
                    DrawCmd::Elements {
                        count,
                        cmd_params:
                            DrawCmdParams {
                                clip_rect,
                                texture_id,
                                // vtx_offset,
                                idx_offset,
                                ..
                            },
                    } => {
                        let clip_rect = [
                            (clip_rect[0] - clip_off[0]) * clip_scale[0],
                            (clip_rect[1] - clip_off[1]) * clip_scale[1],
                            (clip_rect[2] - clip_off[0]) * clip_scale[0],
                            (clip_rect[3] - clip_off[1]) * clip_scale[1],
                        ];

                        if clip_rect[0] < fb_width
                            && clip_rect[1] < fb_height
                            && clip_rect[2] >= 0.0
                            && clip_rect[3] >= 0.0
                        {
                            scissors[0] = Scissor {
                                origin: [
                                    f32::max(0.0, clip_rect[0]).floor() as u32,
                                    f32::max(0.0, clip_rect[1]).floor() as u32,
                                ],
                                dimensions: [
                                    (clip_rect[2] - clip_rect[0]).abs().ceil() as u32,
                                    (clip_rect[3] - clip_rect[1]).abs().ceil() as u32,
                                ],
                            };

                            let tex = self.lookup_texture(texture_id)?;

                            let set = {
                                PersistentDescriptorSet::new(
                                    layout.clone(),
                                    [WriteDescriptorSet::image_view_sampler(
                                        0,
                                        tex.0.clone(),
                                        tex.1.clone(),
                                    )],
                                )?
                            };

                            cmd_buf_builder.bind_pipeline_graphics(self.pipeline.clone());
                            cmd_buf_builder.bind_vertex_buffers(0, vec![vertex_buffer.clone()]);
                            cmd_buf_builder.bind_index_buffer(
                                index_buffer
                                    .clone()
                                    .into_buffer_slice()
                                    .slice(idx_offset as u64..(idx_offset + count) as u64)
                                    .unwrap(),
                            );
                            cmd_buf_builder.bind_descriptor_sets(
                                PipelineBindPoint::Graphics,
                                self.pipeline.layout().clone(),
                                0,
                                set.clone(),
                            );
                            cmd_buf_builder.push_constants(self.pipeline.layout().clone(), 0, pc);

                            cmd_buf_builder.draw_indexed(count as u32, 1, 0, 0, 0)?;
                        }
                    }
                    DrawCmd::ResetRenderState => (), // TODO
                    DrawCmd::RawCallback { callback, raw_cmd } => unsafe {
                        callback(draw_list.raw(), raw_cmd)
                    },
                }
            }
        }
        cmd_buf_builder.end_render_pass()?;

        Ok(())
    }

    pub unsafe fn unsafe_draw_commands(
        &mut self,
        cmd_buf_builder: &mut UnsafeCommandBufferBuilder,
        _queue: Arc<Queue>,
        target: Arc<dyn ImageViewAbstract + 'static>,
        draw_data: &imgui::DrawData,
    ) -> Result<(), RendererError> {
        let fb_width = draw_data.display_size[0] * draw_data.framebuffer_scale[0];
        let fb_height = draw_data.display_size[1] * draw_data.framebuffer_scale[1];
        if !(fb_width > 0.0 && fb_height > 0.0) {
            return Ok(());
        }
        let left = draw_data.display_pos[0];
        let right = draw_data.display_pos[0] + draw_data.display_size[0];
        let top = draw_data.display_pos[1];
        let bottom = draw_data.display_pos[1] + draw_data.display_size[1];

        let pc = shader::vs::ty::VertPC {
            matrix: [
                [(2.0 / (right - left)), 0.0, 0.0, 0.0],
                [0.0, (2.0 / (bottom - top)), 0.0, 0.0],
                [0.0, 0.0, -1.0, 0.0],
                [
                    (right + left) / (left - right),
                    (top + bottom) / (top - bottom),
                    0.0,
                    1.0,
                ],
            ],
        };

        let dims = match ImageAccess::dimensions(&target.image()) {
            ImageDimensions::Dim2d { width, height, .. } => [width, height],
            d => {
                return Err(RendererError::BadImageDimensions(d));
            }
        };

        let viewports = vec![Viewport {
            origin: [0.0, 0.0],
            dimensions: [dims[0] as f32, dims[1] as f32],
            depth_range: 0.0..1.0,
        }];
        let mut scissors = vec![Scissor::default()];

        let clip_off = draw_data.display_pos;
        let clip_scale = draw_data.framebuffer_scale;

        // let vtx_buf_len = draw_data.total_vtx_count as usize;
        // let idx_buf_len = draw_data.total_idx_count as usize;

        let mut vertexes = vec![];
        let mut indexes = vec![];

        for draw_list in draw_data.draw_lists() {
            // update the vertex and index buffers
            vertexes.extend(draw_list.vtx_buffer().iter().map(|&v| Vertex::from(v)));
            indexes.extend(draw_list.idx_buffer().iter().cloned());
        }

        let layout = &self.pipeline.layout().set_layouts()[0];

        let framebuffer = Framebuffer::new(
            self.render_pass.clone(),
            FramebufferCreateInfo {
                attachments: vec![target],
                ..Default::default()
            },
        )?;

        self.framebuffer = Some(framebuffer.clone());
        // cmd_buf_builder.copy_buffer(self.vertex_buffer.clone(), self.vertex_dev_buffer.clone())?;
        // cmd_buf_builder.copy_buffer(self.index_buffer.clone(), self.index_dev_buffer.clone())?;

        unsafe {
            cmd_buf_builder.begin_render_pass(
                &RenderPassBeginInfo {
                    clear_values: vec![None],
                    ..RenderPassBeginInfo::framebuffer(framebuffer)
                },
                SubpassContents::Inline,
            );
        }

        let mut dl_vtx_offset = 0;
        let mut dl_idx_offset = 0;

        self.ds.clear();
        self.buffers.clear();
        for draw_list in draw_data.draw_lists() {
            let vertex_buffer = self
                .vrt_buffer_pool
                .chunk(draw_list.vtx_buffer().iter().map(|&v| Vertex::from(v)))
                .unwrap();
            let index_buffer = self
                .idx_buffer_pool
                .chunk(draw_list.idx_buffer().iter().cloned())
                .unwrap();
            self.buffers.push(vertex_buffer.clone());
            self.buffers.push(index_buffer.clone());

            for cmd in draw_list.commands() {
                match cmd {
                    DrawCmd::Elements {
                        count,
                        cmd_params:
                            DrawCmdParams {
                                clip_rect,
                                texture_id,
                                vtx_offset,
                                idx_offset,
                                ..
                            },
                    } => {
                        let clip_rect = [
                            (clip_rect[0] - clip_off[0]) * clip_scale[0],
                            (clip_rect[1] - clip_off[1]) * clip_scale[1],
                            (clip_rect[2] - clip_off[0]) * clip_scale[0],
                            (clip_rect[3] - clip_off[1]) * clip_scale[1],
                        ];

                        if clip_rect[0] < fb_width
                            && clip_rect[1] < fb_height
                            && clip_rect[2] >= 0.0
                            && clip_rect[3] >= 0.0
                        {
                            scissors[0] = Scissor {
                                origin: [
                                    f32::max(0.0, clip_rect[0]).floor() as u32,
                                    f32::max(0.0, clip_rect[1]).floor() as u32,
                                ],
                                dimensions: [
                                    (clip_rect[2] - clip_rect[0]).abs().ceil() as u32,
                                    (clip_rect[3] - clip_rect[1]).abs().ceil() as u32,
                                ],
                            };

                            let tex = self.lookup_texture(texture_id)?;

                            let set = {
                                PersistentDescriptorSet::new(
                                    layout.clone(),
                                    [WriteDescriptorSet::image_view_sampler(
                                        0,
                                        tex.0.clone(),
                                        tex.1.clone(),
                                    )],
                                )?
                            };
                            self.ds.push(set.clone());

                            unsafe {
                                cmd_buf_builder.pipeline_barrier(&{
                                    use vulkano::sync::*;

                                    let memory_barrier = MemoryBarrier {
                                        source_stages: PipelineStages {
                                            host: true,
                                            ..PipelineStages::none()
                                        },
                                        source_access: AccessFlags {
                                            host_write: true,
                                            ..AccessFlags::none()
                                        },
                                        destination_stages: PipelineStages {
                                            all_graphics: true,
                                            ..PipelineStages::none()
                                        },
                                        destination_access: AccessFlags {
                                            index_read: true,
                                            memory_read: true,
                                            ..AccessFlags::none()
                                        },
                                        ..MemoryBarrier::default()
                                    };

                                    DependencyInfo {
                                        memory_barriers: vec![memory_barrier].into(),
                                        ..Default::default()
                                    }
                                });
                                cmd_buf_builder.bind_pipeline_graphics(&self.pipeline.clone());
                                cmd_buf_builder.bind_vertex_buffers(0, {
                                    let mut builder =
                                        UnsafeCommandBufferBuilderBindVertexBuffer::new();
                                    builder.add(&vertex_buffer);
                                    builder
                                });
                                cmd_buf_builder.bind_index_buffer(&index_buffer, IndexType::U16);
                                cmd_buf_builder.bind_descriptor_sets(
                                    PipelineBindPoint::Graphics,
                                    &self.pipeline.layout(),
                                    0,
                                    [set.inner()].iter().cloned(),
                                    [].iter().map(|d| *d),
                                );
                                cmd_buf_builder.push_constants(
                                    &self.pipeline.layout(),
                                    ShaderStages {
                                        vertex: true,
                                        ..ShaderStages::none()
                                    },
                                    0,
                                    std::mem::size_of::<shader::vs::ty::VertPC>() as u32,
                                    &pc,
                                );
                                cmd_buf_builder.set_viewport(0, viewports.iter().cloned());
                                cmd_buf_builder.set_scissor(0, scissors.iter().cloned());

                                cmd_buf_builder.draw_indexed(
                                    index_buffer.size() as u32 / 2,
                                    1,
                                    0,
                                    0,
                                    0,
                                );
                            }
                        }
                    }
                    DrawCmd::ResetRenderState => (), // TODO
                    DrawCmd::RawCallback { callback, raw_cmd } => unsafe {
                        callback(draw_list.raw(), raw_cmd)
                    },
                }
            }

            dl_vtx_offset += draw_list.vtx_buffer().len();
            dl_idx_offset += draw_list.idx_buffer().len();
        }
        unsafe {
            cmd_buf_builder.end_render_pass();
        }

        Ok(())
    }

    /// Update the ImGui font atlas texture.
    ///
    /// ---
    ///
    /// `ctx`: the ImGui `Context` object
    ///
    /// `device`: the Vulkano `Device` object for the device you want to render the UI on.
    ///
    /// `queue`: the Vulkano `Queue` object for the queue the font atlas texture will be created on.
    pub fn reload_font_texture(
        &mut self,
        ctx: &mut imgui::Context,
        device: Arc<Device>,
        queue: Arc<Queue>,
    ) -> Result<(), RendererError> {
        self.font_texture = Self::upload_font_texture(ctx.fonts(), device, queue)?;
        Ok(())
    }

    /// Get the texture library that the renderer uses
    pub fn textures(&mut self) -> &mut Textures<Texture> {
        &mut self.textures
    }

    fn upload_font_texture(
        mut fonts: imgui::FontAtlasRefMut,
        device: Arc<Device>,
        queue: Arc<Queue>,
    ) -> Result<Texture, RendererError> {
        let texture = fonts.build_rgba32_texture();

        let (image, fut) = ImmutableImage::from_iter(
            texture.data.iter().cloned(),
            ImageDimensions::Dim2d {
                width: texture.width,
                height: texture.height,
                array_layers: 1,
            },
            vulkano::image::MipmapsCount::One,
            Format::R8G8B8A8_SRGB,
            queue.clone(),
        )?;

        fut.then_signal_fence_and_flush()?.wait(None)?;

        let sampler = Sampler::new(device.clone(), SamplerCreateInfo::simple_repeat_linear())?;

        fonts.tex_id = TextureId::from(usize::MAX);
        let image = vulkano::image::view::ImageView::new_default(image).unwrap();
        Ok((image, sampler))
    }

    fn lookup_texture(&self, texture_id: TextureId) -> Result<&Texture, RendererError> {
        if texture_id.id() == usize::MAX {
            Ok(&self.font_texture)
        } else if let Some(texture) = self.textures.get(texture_id) {
            Ok(texture)
        } else {
            Err(RendererError::BadTexture(texture_id))
        }
    }
}
